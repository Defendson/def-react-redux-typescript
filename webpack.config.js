const path = require('path');

const  config = {
	entry: ["./src/view/product"],
	output: {
		path: path.resolve(__dirname, "build"),
		filename: 'bundle.js'
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js", ".jsx"]
	},
	module: {
		rules: [
			{
				test: /.tsx?/,
				// loader: 'ts-loader'
				loader: 'awesome-typescript-loader'
			}
		]
	}
};

module.exports = config;