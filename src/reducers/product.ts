import {Action} from 'redux';
import {State} from '../types/product';
import {GET_PRODUCT, ADD_PRODUCT, UPDATE_PRODUCT, REMOVE_PRODUCT} from '../actions/product';

const productReducer = (state: State, action: Action) => {
	switch (action.type) {
		case GET_PRODUCT:
			return Object.assign({}, state, {
				refundAmount: action.payload
			});
		case ADD_PRODUCT:
			return Object.assign({}, state, {
				refundAmount: action.payload
			});
		case UPDATE_PRODUCT:
			return Object.assign({}, state, {
				refundAmount: action.payload
			});
		case REMOVE_PRODUCT:
			return Object.assign({}, state, {
				refundAmount: action.payload
			});
		default:
			return state;
	}
};

export default productReducer;