import {combineReducers} from 'redux';
import product from './product';

const rootReducer:any = combineReducers({
	product
});

export default rootReducer;