import * as React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import Header from '../containers/header';
import configureStore from '../stores/product';

const initialState = {
	product: {
		name: 'car',
		id: 10,
		price: 30000
	}
};

const store = configureStore(initialState);

class Hello extends React.PureComponent<React.ReactType> {
	render() {
		return (
			<Provider store={store}>
				<div>
					<Header/>
				</div>
			</Provider>
		)
	}
}

render(
	<Hello/>,
	document.getElementById('react-root')
);
