import {createStore, applyMiddleware, Store} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

import {State} from '../types/product';
import productReducer from '../reducers/product';

const configureStore = (initialState: State): Store<State> => {
	const store = createStore(productReducer, initialState, applyMiddleware(
		promiseMiddleware()
	));

	return store;
};


export default configureStore;