import {Action} from 'redux';

export type Product = {
	name?: string,
	id?: number,
	price?: number,
}

export type State = {
	product?: Product,
	getProduct?(productId: number): Action,
	addProduct?(product: Product[]): Action,
	updateProduct?(product: Product[]): Action,
	removeProduct?(product: Product[]): Action
}

type Props = {
	product?: Product,
	getProduct?(productId: number): Action,
	addProduct?(product: Product[]): Action,
	updateProduct?(product: Product[]): Action,
	removeProduct?(product: Product[]): Action
};