import * as React from 'react';
import {connect} from 'react-redux';

class Header extends React.PureComponent<React.ReactType> {
	render() {
		return (
			<h1>This is a HEADER!</h1>
		)
	}
}
//
// // export default Header;
//
// export default connect((store: any) => ({
// 	product: store.product
// }))(Header);

export default Header;