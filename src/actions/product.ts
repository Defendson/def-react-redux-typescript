import {Action} from 'redux';

export const GET_PRODUCT = 'GET_PRODUCT';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';

export const getProduct = (productId: number): Action => ({
	type: GET_PRODUCT, payload: productId
});

export const addProduct = (product: any[]): Action => ({
	type: ADD_PRODUCT, payload: product
});

export const updateProduct = (product: any[]): Action => ({
	type: UPDATE_PRODUCT, payload: product
});

export const removeProduct = (productId: number): Action => ({
	type: REMOVE_PRODUCT, payload: productId
});