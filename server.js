const express = require('express');
const path = require('path');
const app = express();

app.use('/build', express.static(path.join(__dirname, 'build')));
app.get('/', (req, res) => {
	console.log('> somebody comes here');
	res.sendFile(path.join(__dirname, 'index.html'))
});

app.listen(3333, () => {
	console.log('Server was started on http://localhost:3333/')
});